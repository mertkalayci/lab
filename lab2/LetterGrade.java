public class LetterGrade  {
    public static void main(String[] args){
        int note = Integer.parseInt(args[0]);
        if (note > 100)
            System.out.println("Number is must be less than 100");
        else if (note <= 100 && note >=90)
            System.out.println("A");
        else if (note <90 && note >= 80)
            System.out.println("B");
        else if (note <80 && note >= 70)
            System.out.println("C");
        else if (note <70 && note >= 60)
            System.out.println("D");
        else if (note <60 && note >=0)
            System.out.println("F");
        else if (note < 0)
            System.out.println("Number is must be bigger than 0");
    }
}