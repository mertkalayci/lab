import java.io.IOException;
import java.sql.SQLOutput;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
		int count = 0;

		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.println("Can you guess it: ");

		int guess = reader.nextInt(); //Read the user input

		while(guess != number && guess != -1) {
			System.out.println("Sorry!");
			count += 1;

			if(guess < number)
				System.out.println("Mine is greater than your guess");
			else
				System.out.println("Mine is less than your guess");
			System.out.println("Type -1 to quit or guess another");

			guess = reader.nextInt();
		}

		if (guess == number){
			System.out.println("Congratulations!");
			System.out.println("You found the answer in " + count +" try.");
		}


		reader.close(); //Close the resource before exiting
	}


}