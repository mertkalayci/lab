public class FindPrimes {
    public static void main(String[] args){
        int max = Integer.parseInt(args[0]);
        //print the numbers less than max
        //for each number less than max
        for(int number = 2; number < max; number++){

            int divisor =2;

            boolean isPrime = true;

            while(divisor < number && isPrime) {
                if(number % divisor == 0)
                    isPrime = false;
                divisor++;
            }

            if (isPrime)
                System.out.print(number + " ");
        }
    }
}